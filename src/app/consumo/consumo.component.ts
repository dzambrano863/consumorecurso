import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { DependenciaPasto } from '../DependenciaPasto';
import { interval } from 'rxjs';


@Component({
  selector: 'app-consumo',
  templateUrl: './consumo.component.html',
  styleUrls: ['./consumo.component.css']
})
export class ConsumoComponent  implements OnInit  {
  arregloDependencia:DependenciaPasto[]=[];
  opciones:number[]=[5,10,50,172]
  estilosArray=["table-primary","table-secondary","table-success","table-danger","table-warning","table-info","table-light","table-dark"];
  tableEstilos="";
  selected:number=5;
  constructor(private dependencia:ServiceService){
    this.consumo(this.selected);
  }
  consumo(numero:number){
    var num=numero;
     this.dependencia.peticion().subscribe(resp=>this.arregloDependencia= resp.splice(1,num))
   
  }
  ngOnInit(){
     this.colores();
  }

  colores(){
    const intervalo=interval(1000); 
    intervalo.subscribe(resp=>{
 
     return this.tableEstilos=this.estilosArray[Math.floor((Math.random() * (7 - 0 + 0)) + 0)];
    
    })
   
    
  }


 
}
