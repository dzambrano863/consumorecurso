import { Injectable } from '@angular/core';
import {   HttpClient  } from '@angular/common/http';
import { DependenciaPasto } from './DependenciaPasto';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient) { }

 peticion(){
  return this.http.get<DependenciaPasto[]>("https://www.datos.gov.co/resource/4din-bcuq.json");
 }

}
