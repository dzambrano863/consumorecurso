
export interface DependenciaPasto {
    dependencia: string;
    rea:         string;
    telef_nico:  string;
    extensi_n:   string;
    direcci_n:   string;
    color:string;
}