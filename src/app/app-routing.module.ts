

import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsumoComponent } from './consumo/consumo.component';



const routes: Routes = [
  {
 
      path:'consumo',
      component:ConsumoComponent

  },
  {
    path:'**',
    redirectTo:"consumo"
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
